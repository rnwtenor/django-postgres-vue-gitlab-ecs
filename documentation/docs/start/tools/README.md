# Tools used in this project

This is a list of the tools used in this project.


<technology image="django.jpg" />

## [Django](https://www.djangoproject.com/)

### The Web framework for perfectionists with deadlines

Django is a Python-based free and open-source web framework, which follows the model-template-view architectural pattern. It is maintained by the Django Software Foundation, an independent organization established as a 501 non-profit. Django's primary goal is to ease the creation of complex, database-driven websites.

[Read more about Django](https://www.djangoproject.com/)


<technology image="vue.png" />

## [Vue.js](https://vuejs.org/)

### The Progressive JavaScript Framework

Vue (pronounced /vjuː/, like view) is a progressive framework for building user interfaces. Unlike other monolithic frameworks, Vue is designed from the ground up to be incrementally adoptable. The core library is focused on the view layer only, and is easy to pick up and integrate with other libraries or existing projects. On the other hand, Vue is also perfectly capable of powering sophisticated Single-Page Applications when used in combination with modern tooling and supporting libraries.

[Read more about Vue.js](https://vuejs.org/)

<technology image="gitlab.svg" />

## [GitLab](https://gitlab.com/)

### The first single application for the entire DevOps lifecycle

From project planning and source code management to CI/CD and monitoring, GitLab is a complete DevOps platform, delivered as a single application.

[Read more about GitLab](https://gitlab.com/)


<technology image="postgres.png" />

## [PostgreSQL](https://www.postgresql.org/)

### The world's most advanced open source database

PostgreSQL, also known as Postgres, is a free and open-source relational database management system emphasizing extensibility and technical standards compliance. It is designed to handle a range of workloads, from single machines to data warehouses or Web services with many concurrent users.

[Read more about PostgreSQL](https://www.postgresql.org/)

<technology image="ecs.png" />

## [Amazon ECS](https://aws.amazon.com/ecs/)

### Run containerized applications in production

Amazon Elastic Container Service (Amazon ECS) is a highly scalable, high-performance container orchestration service that supports Docker containers and allows you to easily run and scale containerized applications on AWS.

[Read more about ECS](https://aws.amazon.com/ecs/)


::: warning Incomplete
This section is incomplete.
:::